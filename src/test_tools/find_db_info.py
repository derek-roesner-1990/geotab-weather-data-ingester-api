﻿import arrow
import MySQLdb

def get_database_connection():

	HOST = os.environ["DB_HOST"]
	DATABASE_NAME = os.environ["DB_DATABASE"]
	USERNAME = os.environ["DB_USERNAME"]
	PASSWORD = os.environ["DB_PASSWORD"]

	db_connection = None

	try:

		db_connection=MySQLdb.connect(host=HOST, db=DATABASE_NAME, user=USERNAME, passwd=PASSWORD)

	except Exception as e:
		raise Exception("An error occured while establishing a connection with the mySQL database.\nDetails:\n  {}".format(e))

	return db_connection
	
def execute_sql(db_connection, sql):

	try:

		row_ids = []

		# See this resource more info on the use of this statement.
		# https://stackoverflow.com/questions/5669878/when-to-close-cursors-using-mysqldb
		with closing(db_connection.cursor()) as cursor:

			for params in sql['params']:
				cursor.execute(sql['statement'], params)
				
	except Exception as e:
		raise Exception("An error occured while executing the following SQL statement.\nStatement Info:\n  {}\nDetails:\n  {}".format(sql,e))

	return row_ids

def check_entry_amounts(timespan):
	db_con = get_database_connection();
	
	
	
def main():
	check_entry_amounts()
	
	
	
# More info at https://stackoverflow.com/questions/419163/what-does-if-name-main-do#419185
if __name__ == '__main__':
    main()