# Flask's Official Documentation - http://flask.pocoo.org/docs

# To install MYSQLdb within Ubuntu run the following commands in the Terminal
# apt-get install python-dev libmysqlclient-dev
# pip install MySQL-python
# More Info - https://codeinthehole.com/tips/how-to-set-up-mysql-for-python-on-ubuntu/
import arrow
import json
import MySQLdb
import os
import sys
from contextlib import closing
from flask import Flask, request, abort, make_response
from logging.config import dictConfig

# Enabling the Flask logger to output it's contents to the USWGI log file.
# See the resources below for more details:
# https://stackoverflow.com/questions/51318988/why-flask-logger-does-not-log-in-docker-when-using-uwsgi-in-front
# https://docs.python.org/2/library/logging.config.html
dictConfig({
    'version': 1,
    'formatters': {'default': {
        'format': '%(message)s',
    }},
    'handlers': {'wsgi': {
        'class': 'logging.StreamHandler',
        'formatter': 'default'
    }},
    'root': {
        'level': 'DEBUG',
        'handlers': ['wsgi']
    }
})

app = Flask(__name__)

@app.route('/', methods=['GET'])
def index():
	return 'Running'

@app.route('/ingest_temperature_data/', methods=['POST'])
def ingest_temperature_data():

	try:

		app.logger.info('Temperature data received.')

		if not request.json:
			abort(400)

		raw_data = json.loads(request.json)
		#app.logger.info(raw_data)

		app.logger.info("Amount of data objects recieved {0}.".format(len(raw_data)))

		processed_data = process_temperature_data(raw_data)
		#app.logger.info(processed_data)

		attempt_database_inserts(processed_data)

	except Exception as e:
		message = "An error occured while ingesting the incoming temperature data.\nDetails:\n  {}".format(e)
		app.logger.info(message)
		abort(make_response(message, 500))

	message = "All temperature data successfully inserted into the associated database."
	app.logger.info(message)
	return make_response(message, 200)

@app.route('/ingest_pressure_data/', methods=['POST'])
def ingest_pressure_data():

	try:

		app.logger.info("Barometric pressure data received.")

		if not request.json:
			abort(400)

		raw_data = json.loads(request.json)
		#app.logger.info(raw_data)

		app.logger.info("Amount of data objects recieved {0}.".format(len(raw_data)))

		processed_data = process_pressure_data(raw_data)
		#app.logger.info(processed_data)

		attempt_database_inserts(processed_data)

	except Exception as e:
		message = "An error occured while ingesting the incoming barometric pressure data.\nDetails:\n  {}".format(e)
		app.logger.info(message)
		abort(make_response(message, 500))

	message = "All barometric pressure data successfully inserted into the associated database."
	app.logger.info(message)
	return make_response(message, 200)

def cast_to_integer(decimal,percision):

	MAX_PERCISION = len(str(sys.maxint))

	if percision <= 0  or percision > MAX_PERCISION:
		raise ValueError("The value provided to the percision argument of cast_to_integer() is invalid.\n  The provided value must be greater than 0 and less than {}.".format(MAX_PERCISION+1))

	# ** is the exponentiation operator
	i = 10**percision
	decimal *= i

	return int(decimal)

def get_geohash_center_point(a,b):
	center = ((a+b)/2)
	center = cast_to_integer(center,percision=5)
	return center

def get_datetime_string(format_string, date, hours=0, minutes=0, seconds=0):
	arrowDateTime = arrow.get(date)
	arrowDateTime = arrowDateTime.replace(hour=hours, minute=minutes, second=seconds)
	return arrowDateTime.format(format_string)

def process_temperature_data(data_list):
	
	processed_data_list = []
	AIR_TEMPERATURE_FIELD_ID = 209

	try:

		for data in data_list:
			
			processed_data = {
							'tracking_record_data': { 'geohash_longitude_center': None, 'geohash_latitude_center': None, 'record_date_time': None, 'unit_id': None },
							'tracking_record_field_data': []
							 }

			processed_data['tracking_record_data']['geohash_longitude_center'] = get_geohash_center_point(data['Longitude_SW'],data['Longitude_NE'])
			processed_data['tracking_record_data']['geohash_latitude_center'] = get_geohash_center_point(data['Latitude_SW'], data['Latitude_NE'])
			processed_data['tracking_record_data']['record_date_time'] = get_datetime_string("YYYY-MM-DD HH:mm:ss", data['UTC_Date'], hours=int(data['UTC_Hour']))
			processed_data['tracking_record_data']['unit_id'] = get_datetime_string("YYYYMMDDHH", data['UTC_Date'], hours=int(data['UTC_Hour']))

			tracking_record_field_data = { 'tracking_record_id': None, 'field_id': None, 'value': None, 'record_date_time': None }
			tracking_record_field_data['field_id'] = AIR_TEMPERATURE_FIELD_ID
			tracking_record_field_data['value'] = data['Temperature_C']
			tracking_record_field_data['record_date_time'] = get_datetime_string("YYYY-MM-DD HH:mm:ss", data['UTC_Date'], hours=int(data['UTC_Hour']))
			tracking_record_field_data['unit_id'] = get_datetime_string("YYYYMMDDHH", data['UTC_Date'], hours=int(data['UTC_Hour']))

			processed_data['tracking_record_field_data'].append(tracking_record_field_data)
			processed_data_list.append(processed_data)

	except Exception as e:
		raise Exception("An error occured while processing the received temperature data.\nDetails:\n  {}".format(e))

	return processed_data_list

def process_pressure_data(data_list):
	
	processed_data_list = []
	ATMOSPHERIC_PRESSURE_FIELD_ID = 10101
	SEA_LEVEL_PRESSURE_FIELD_ID = -2

	try:

		for data in data_list:
			
			processed_data = { 
							'tracking_record_data': { 'geohash_longitude_center': None, 'geohash_latitude_center': None, 'record_date_time': None, 'unit_id': None },
							'tracking_record_field_data': []
							 }

			processed_data['tracking_record_data']['geohash_longitude_center'] = get_geohash_center_point(data['Longitude_SW'],data['Longitude_NE'])
			processed_data['tracking_record_data']['geohash_latitude_center'] = get_geohash_center_point(data['Latitude_SW'], data['Latitude_NE'])
			processed_data['tracking_record_data']['record_date_time'] = get_datetime_string("YYYY-MM-DD HH:mm:ss", data['UTC_Date'], hours=int(data['UTC_Hour']))
			processed_data['tracking_record_data']['unit_id'] = get_datetime_string("YYYYMMDDHH", data['UTC_Date'], hours=int(data['UTC_Hour']))

			tracking_record_field_data = { 'tracking_record_id': None, 'field_id': None, 'value': None, 'record_date_time': None, 'unit_id': None }
			tracking_record_field_data['field_id'] = ATMOSPHERIC_PRESSURE_FIELD_ID
			tracking_record_field_data['value'] = data['Pressure']
			tracking_record_field_data['record_date_time'] = get_datetime_string("YYYY-MM-DD HH:mm:ss", data['UTC_Date'], hours=int(data['UTC_Hour']))
			tracking_record_field_data['unit_id'] = get_datetime_string("YYYYMMDDHH", data['UTC_Date'], hours=int(data['UTC_Hour']))
			processed_data['tracking_record_field_data'].append(tracking_record_field_data)

			tracking_record_field_data = { 'tracking_record_id': None, 'field_id': None, 'value': None, 'record_date_time': None, 'unit_id': None }
			tracking_record_field_data['field_id'] = SEA_LEVEL_PRESSURE_FIELD_ID
			tracking_record_field_data['value'] = data['MSLP']
			tracking_record_field_data['record_date_time'] = get_datetime_string("YYYY-MM-DD HH:mm:ss", data['UTC_Date'], hours=int(data['UTC_Hour']))
			tracking_record_field_data['unit_id'] = get_datetime_string("YYYYMMDDHH", data['UTC_Date'], hours=int(data['UTC_Hour']))
			processed_data['tracking_record_field_data'].append(tracking_record_field_data)

			processed_data_list.append(processed_data)

	except Exception as e:
		raise Exception("An error occured while processing the received temperature data.\nDetails:\n  {}".format(e))
	
	return processed_data_list

def create_tracking_record_inserts(data_list):

	try:

		insert_statements = { 'statement': None, 'params': None }

		insert_statements['statement'] = "INSERT INTO tracking_records ( longitude, latitude, record_time, unit_id, created_at ) values (%s,%s,%s,%s,%s)"

		insert_statements['params'] = [ ( data['tracking_record_data']['geohash_longitude_center'],
										data['tracking_record_data']['geohash_latitude_center'],
										data['tracking_record_data']['record_date_time'],
										data['tracking_record_data']['unit_id'],
										arrow.utcnow().format('YYYY-MM-DD HH:mm:ss')) for data in data_list ]

	except Exception as e:
		raise Exception("An error occured while creating SQL insert statements for the tracking_records table.\nDetails:\n  {}".format(e))

	return insert_statements


def create_tracking_record_field_data_inserts(data_list):
	
	try:

		insert_statements = { 'statement': None, 'params': [] }

		insert_statements['statement'] = "INSERT INTO tracking_record_field_data ( tracking_record_id, field_id, value, record_time, unit_id, created_at ) values (%s,%s,%s,%s,%s,%s)"

		#loop through data_list and create an insert statement for each entry within the 'tracking_record_field_data' array

		for data in data_list:
			for tracking_record_field_data in data['tracking_record_field_data']:
				insert_statements['params'].append((tracking_record_field_data['tracking_record_id'],
													tracking_record_field_data['field_id'],
													tracking_record_field_data['value'],
													tracking_record_field_data['record_date_time'],
													tracking_record_field_data['unit_id'],
													arrow.utcnow().format('YYYY-MM-DD HH:mm:ss')))

	except Exception as e:
		raise Exception("An error occured while creating SQL insert statements for the tracking_record_field_data table.\nDetails:\n  {}".format(e))

	return insert_statements

def get_database_connection():

	HOST = os.environ["DB_HOST"]
	DATABASE_NAME = os.environ["DB_DATABASE"]
	USERNAME = os.environ["DB_USERNAME"]
	PASSWORD = os.environ["DB_PASSWORD"]

	db_connection = None

	try:

		db_connection=MySQLdb.connect(host=HOST, db=DATABASE_NAME, user=USERNAME, passwd=PASSWORD)

	except Exception as e:
		raise Exception("An error occured while establishing a connection with the mySQL database.\nDetails:\n  {}".format(e))

	return db_connection


def attempt_database_inserts(data_list):

	try:

		tracking_record_inserts = create_tracking_record_inserts(data_list)

		db_connection = get_database_connection()

		row_ids = execute_sql(db_connection, tracking_record_inserts, collect_row_ids = True)
		#print row_ids

		# Associating the records inserted into the 'tracking_records' table with the related records to be inserted into the 'tracking_record_field_data' table.
		i=0
		for row_id in row_ids:
			for tracking_record_field_data in data_list[i]['tracking_record_field_data']:
				tracking_record_field_data['tracking_record_id'] = row_id
			i +=1

		tracking_record_field_data_inserts = create_tracking_record_field_data_inserts(data_list)
		#app.logger.info(tracking_record_field_data_inserts)

		execute_sql(db_connection, tracking_record_field_data_inserts, collect_row_ids = False)

		# Committing all insert statements allowing them to take effect within the database.
		db_connection.commit()
		db_connection.close()

	except Exception as e:
		# Rolling back all insert statements to prevent them from taking effect within the database.
		if(db_connection.open == 1):
			db_connection.rollback()
			db_connection.close()

		raise Exception("An error occured while inserting data into the associated database.\nDetails:\n{}".format(e))

def execute_sql(db_connection, sql, collect_row_ids):

	try:

		row_ids = []

		# See this resource more info on the use of this statement.
		# https://stackoverflow.com/questions/5669878/when-to-close-cursors-using-mysqldb
		with closing(db_connection.cursor()) as cursor:

			if(collect_row_ids == True):
				for params in sql['params']:
					cursor.execute(sql['statement'], params)
					row_ids.append(int(cursor.lastrowid))
			else:
				cursor.executemany(sql['statement'], sql['params'])

	except Exception as e:
		raise Exception("An error occured while executing the following SQL statement.\nStatement Info:\n  {}\nDetails:\n  {}".format(sql,e))

	return row_ids

if __name__ == '__main__':
	app.run(debug=True,host='0.0.0.0')

