FROM ubuntu:latest
RUN apt-get update -y #redo
RUN apt-get install -y python-pip python-dev build-essential libmysqlclient-dev libpcre3 libpcre3-dev
COPY required_python_modules.txt /app/required_python_modules.txt
WORKDIR /app
RUN pip install -r required_python_modules.txt
COPY . /app
EXPOSE 5000/tcp
CMD ["uwsgi", "--ini", "uwsgi/uwsgi_flask.ini"]

