[Syntax Cheat Sheet for Markup Langauge](https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)  
[Tool For Previewing Changes To This Document](https://dillinger.io/)  

# Description of Components

Four separate components interact with each other to enable the data ingester to retrieve, process and store the intended weather data.  

These components are:  
1. [The 'geotab-intelligence.Weather' data set](https://data.geotab.com/)  
2. [The GeoTab Weather Data Ingester](https://bitbucket.org/weathertx/geotab_weather_data_ingester)  
3. [The GeoTab Weather Data Ingester API](https://bitbucket.org/weathertx/geotab_weather_data_ingester_api)  
4. A mySQL database for storing the ingested data.  

# GeoTab Weather Data Ingester  
- **Interacts With**  
The 'geotab-intelligence.Weather' data set.  
The GeoTab Weather Data Ingester API.  

***
This component is intended to be ran within a Docker container and the created container is intended to be ran within a Kubernetes Cluster.  
See the section "Building and Deployment" for more information.
***  

The purpose of the Data Ingester is to retrieve:  
**air temperature, atmospheric pressure and sea level pressure**  
**longtitude, latitude, date and time information for each reading**  

This data is retrieved from the Google BigQuery data set named geotab-intelligence.Weather which is made available by GeoTab.  
Specifically the Temperature and Pressure tables within this data set will be queried to retrieve the following information.  

**Data Retrieved From Temperature Table:**  
Longitude_SW  
Longitude_NE  
Latitude_SW  
Latitude_NE  
Temperature_C  - *Air Temperature in Celcius*  
UTC_Date  
UTC_Hour  

**Data Retrieved From Pressure Table:**  
Longitude_SW  
Longitude_NE  
Latitude_SW  
Latitude_NE  
Pressure  - *Atmospheric Pressure*  
MSLP  - *Mean Sea-Level Adjusted Pressure*  
UTC_Date  
UTC_Hour  

Retrieval is performed once a hour using all available data from the United States related to the previous hour.  
Scheduling of the data retrieval is handled by the Python Script ['process_scheduler.py'](https://bitbucket.org/weathertx/geotab_weather_data_ingester/src/master/src/process_scheduler.py).  

The retrieved information will be sent to the GeoTab Weather Data Ingester API in batches of 1000.  
This value is configurable using the MAX_GROUP_SIZE variable contained within the Python Script ['data_ingester.py'](https://bitbucket.org/weathertx/geotab_weather_data_ingester/src/master/src/data_ingester.py).  
# GeoTab Weather Data Ingester API  
- ***Interacted With By***  
The GeoTab Weather Data Ingester.  

- **Interacts With**  
The mySQL database for storing the ingested data.  

***
This component is intended to be ran within a Docker container and the created container is intended to be ran within a Kubernetes Cluster.  
See the section "Building and Deployment" for more information.
***  

Once retrieved from GeoTab's data set, the weather data will be sent to the accompanying API where it will be processed and then stored within a mySQL database.  

#### Processing of Retrieved Data  
Please see the functions *process_temperature_data() and process_pressure_data()* within [api.py](https://bitbucket.org/weathertx/geotab_weather_data_ingester_api/src/master/src/api.py) for information about the processing that is performed.  

#### Storage of Processed Data  
Please see the function *attempt_database_inserts()* within [api.py](https://bitbucket.org/weathertx/geotab_weather_data_ingester_api/src/master/src/api.py) and the upcoming section describing the accompanying mySQL database for information about the storage of the processed data.  

## mySQL Database  
- ***Interacted With By***  
The GeoTab Weather Data Ingester API.  

***
This component is intended to be ran within a Docker container and the created container is intended to be ran within a Kubernetes Cluster.   
See the section "Building and Deployment" for more information.
***  

The processed data will be inserted into the *tracking_records* and *tracking_record_field_data* tables as described below.  

**tracking_records**  
A row is added to this table for each retrieved latitude and longitude reading.  
The columns *proper_mapping and set* receive the value of NULL.  

**Sample Data**  
```
+----+------------+---------------------+----------+-----------+---------------------+------------+------+
| id | unit_id    | record_time         | latitude | longitude | created_at          | updated_at | set  |
+----+------------+---------------------+----------+-----------+---------------------+------------+------+
|  1 | 2018083016 | 2018-08-30 16:00:00 |  2130867 | -15788520 | 2018-08-30 18:25:25 | NULL       | NULL |
|  2 | 2018083016 | 2018-08-30 16:00:00 |  2136909 | -15792640 | 2018-08-30 18:25:25 | NULL       | NULL |
+----+------------+---------------------+----------+-----------+---------------------+------------+------+
```  

**Table Defintion**  
```
+-------------+---------------------+------+-----+---------------------+----------------+
| Field       | Type                | Null | Key | Default             | Extra          |
+-------------+---------------------+------+-----+---------------------+----------------+
| id          | bigint(20) unsigned | NO   | PRI | NULL                | auto_increment |
| unit_id     | int(11)             | YES  | MUL | NULL                |                |
| record_time | datetime            | NO   | PRI | 0000-00-00 00:00:00 |                |
| latitude    | int(11)             | YES  |     | NULL                |                |
| longitude   | int(11)             | YES  |     | NULL                |                |
| created_at  | datetime            | YES  |     | NULL                |                |
| updated_at  | datetime            | YES  |     | NULL                |                |
| set         | bigint(20)          | YES  |     | NULL                |                |
+-------------+---------------------+------+-----+---------------------+----------------+
```  
**tracking_record_field_data**  
A row is added to this table for each reading type of temperature or barometric pressure.  

Each row is related to a specific row in the *tracking_records* table via the column *tracking_record_id*.  
The value of *tracking_record_id* is the value assigned to the *id* column of the related row within the *tracking_records* table.  

Each row is also associated with the *equipment* table via the column *field_id*.  
The value assigned to *field_id* is determined by the reading type being collected.  

**Values By Reading Type:**  
Air Temperature: 209  
Atmospheric Pressure: 10101  
Mean Sea-Level Adjusted Pressure: -2  

The column *reportability* receives the value of NULL.  

**Sample Data**  
```
+----+--------------------+------------+----------+---------------------+--------+---------------+---------------------+------------+
| id | tracking_record_id | unit_id    | field_id | record_time         | value  | reportability | created_at          | updated_at |
+----+--------------------+------------+----------+---------------------+--------+---------------+---------------------+------------+
|  1 |                  1 | 2018083016 |      209 | 2018-08-30 16:00:00 | 23.4   |          NULL | 2018-08-30 18:25:25 | NULL       |
|  2 |                  2 | 2018083016 |    10101 | 2018-08-30 16:00:00 | 102000 |          NULL | 2018-08-30 18:25:26 | NULL       |
|  3 |                  2 | 2018083016 |       -2 | 2018-08-30 16:00:00 | 102300 |          NULL | 2018-08-30 18:25:26 | NULL       |
+----+--------------------+------------+----------+---------------------+--------+---------------+---------------------+------------+
```  

**Table Definition**  
```
+--------------------+---------------------+------+-----+---------------------+----------------+
| Field              | Type                | Null | Key | Default             | Extra          |
+--------------------+---------------------+------+-----+---------------------+----------------+
| id                 | bigint(20) unsigned | NO   | PRI | NULL                | auto_increment |
| tracking_record_id | bigint(20)          | YES  | MUL | NULL                |                |
| unit_id            | int(11)             | YES  |     | NULL                |                |
| field_id           | int(11)             | YES  | MUL | NULL                |                |
| record_time        | datetime            | NO   | PRI | 0000-00-00 00:00:00 |                |
| value              | varchar(255)        | YES  |     | NULL                |                |
| reportability      | smallint(6)         | YES  |     | NULL                |                |
| created_at         | datetime            | YES  |     | NULL                |                |
| updated_at         | datetime            | YES  |     | NULL                |                |
+--------------------+---------------------+------+-----+---------------------+----------------+
```  
# Building and Deployment

The described components must be deployed in the following order.  
1. [mySQL database](https://gitlab.com/derek-roesner-1990/geotab_weather_data_ingester_dev_db).  
2. [GeoTab Weather Data Ingester API](https://gitlab.com/derek-roesner-1990/geotab-weather-data-ingester-api/) 
3. [GeoTab Weather Data Ingester](https://gitlab.com/derek-roesner-1990/geotab-data-ingester)


## GeoTab Weather Data Ingester API  
### When Developing Locally  
**Build the image to run within the Docker Container.**  
*The -t flag is used to apply the tag of 'v1' to the created image.  
See the [DockerFile](https://gitlab.com/derek-roesner-1990/geotab-weather-data-ingester-api/-/blob/main/Dockerfile) for the image definition.*  
```
docker build -t geotab-data-ingester-api:v1 .
```  
**Connect kubectl to the cluster hosted by Minikube.**  
*[Installation instructions for Minikube.](https://kubernetes.io/docs/tasks/tools/install-minikube/)*  
```
kubectl config use-context minikube
```  
**Ensure kubectl is connected to the intended cluster.**  
*The output should be 'minikube'.*  
```
kubectl config current-context
```  
**If needed, create the namespace 'mesonet-legacy'.**  
*See [namespace.yaml](https://gitlab.com/derek-roesner-1990/geotab-weather-data-ingester-api/-/blob/main/kubernetes/namespace.yaml) for the namespace definition.*  
```
kubectl create -f kubernetes/namespace.yaml
```  
**Deploy within the Kubernetes Cluster hosted by Minikube.**  
***  
If a deployment with the same name exists within the cluster the existing deployment will be replaced.  
Use apply instead of create to update the defintion of a deployment.  
***    
*See [deployment-local-dev.yaml](https://gitlab.com/derek-roesner-1990/geotab-weather-data-ingester-api/-/blob/main/kubernetes/deployment-local-dev.yaml) for the deployment definition.*  
```
kubectl create -f kubernetes/deployment-local-dev.yaml
```  
**Create a service to allow the Data Ingester to contact the API using its internal IP address.**  
*See [service.yaml](https://gitlab.com/derek-roesner-1990/geotab-weather-data-ingester-api/-/blob/main/kubernetes/service.yaml) for the service definition.*  
```
kubectl create -f kubernetes/service.yaml
```  
**Access the minikube dashboard to inspect the created content.**  
```
minikube dashboard
```  
### For The Cloud Environment  
**Login to the Kubernetes Cluster associated with the desired project.**  
**If needed, create a new cluster.**  
[More information on Kubernetes Clusters](https://kubernetes.io/docs/tutorials/kubernetes-basics/create-cluster/cluster-interactive/)

**Build the image to run within the Docker Container.**  
*The -t flag is used to apply the tag of 'v1' to the created image.  
See the [DockerFile](https://gitlab.com/derek-roesner-1990/geotab-weather-data-ingester-api/-/blob/main/Dockerfile) for the image definition.*  
[More information on Docker Containers](https://docs.docker.com/get-started/02_our_app/)    
```
docker build -t 'container_name' mesonet-geotab-data-ingester:v1 .
```   

**If needed, create a kubeconfig entry for the staging Kubernetes Cluster.**  
[More information on available timezones](https://cloud.google.com/compute/docs/regions-zones/)  
```
gcloud container clusters get-credentials cluster-1 --zone us-central1-a --project 'project_name'
``` 
**Connect kubectl to the Kubernetes Cluster.**  
```
kubectl config use-context 'cluster_name'
```  
**Ensure kubectl is connected to the intended cluster.**   
```
kubectl config current-context
```  
**If needed, create the namespace 'mesonet-legacy'.**  
*See [namespace.yaml](https://bitbucket.org/weathertx/geotab_weather_data_ingester_api/src/master/kubernetes/namespace.yaml) for the namespace definition.*  
```
kubectl create -f kubernetes/namespace.yaml
```  
**Deploy within the staging Kubernetes Cluster.**  
***  
If a deployment with the same name exists within the cluster the existing deployment will be replaced. Use apply instead of create to update the defintion of a deployment.  
***  
*See [deployment.yaml](https://bitbucket.org/weathertx/geotab_weather_data_ingester_api/src/master/kubernetes/deployment.yaml) for the deployment definition.*  
```
kubectl create -f kubernetes/deployment.yaml
```  
**Create a service to allow the Data Ingester to contact the API using its internal IP address.** *See [service.yaml](https://bitbucket.org/weathertx/geotab_weather_data_ingester_api/src/master/kubernetes/service.yaml) for the service definition.*  
```
kubectl create -f kubernetes/service.yaml
```  
**Retrieve the needed pod_id from the related pod.**  
[More information on Kubernetes Pods.](https://kubernetes.io/docs/concepts/workloads/pods/)
```
kubectl get pods
kubectl describe pods my-pod
```

**Navigate to the previously created Kubernetes Cluster to inspect the content.**

# Additional Resources  
[Information on temperature data from GeoTab.](https://data.geotab.com/weather/temperature)  
[Information on barometric pressure data from GeoTab.](https://data.geotab.com/weather/temperature)  
[Reference for Python Google BigQuery Client Library.](https://googlecloudplatform.github.io/google-cloud-python/latest/bigquery/reference.html)  
[Usage Guide for Python Google BigQuery Client Library.](https://googlecloudplatform.github.io/google-cloud-python/latest/bigquery/usage.html)  
